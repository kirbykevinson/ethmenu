#!/usr/bin/env python3

import ethmenu, ethmenu.cache

__title__ = f"{ethmenu.__title__}-desktop"

def main():
	"""
	Launches the menu with the list of programs specified in the XDG
	desktop entries and executes the chosen one.
	"""
	
	import subprocess
	
	subprocess.run(ethmenu.real_main(desktop_option_gen), shell=True)

def desktop_option_gen():
	"""
	The option generator functionally stolen from i3-dmenu-desktop.
	
	# Yields
	
	The options of programs in the XDG desktop entries.
	"""
	
	import os, json, configparser, re, xdg
	
	cache_path = xdg.XDG_CACHE_HOME / f"{__title__}"
	
	local_app_dir = xdg.XDG_DATA_HOME / "applications"
	
	cached_dirs = [
		cached_dir / "applications"
		for cached_dir in xdg.XDG_DATA_DIRS
	]
	
	if local_app_dir not in cached_dirs:
		cached_dirs.insert(0, local_app_dir)
	
	cache_is_relevant = ethmenu.cache.check(cache_path, cached_dirs)
	
	if cache_is_relevant:
		with open(cache_path, "r") as cache:
			for option in json.load(cache):
				yield ethmenu.Option(
					caption=option.get("caption"),
					value=option.get("value"),
					icon=option.get("icon")
				)
	else:
		options = []
		
		field_code_stripper = re.compile("%[fFuUdDnNickvm]")
		percent_sign_normalizer = re.compile("%%")
		
		for entry in ethmenu.cache.files(cached_dirs):
			if os.path.splitext(entry)[1] == ".desktop":
				parser = configparser.ConfigParser(strict=False, interpolation=None)
				
				parser.read(entry)
				
				if not parser.has_section("Desktop Entry"):
					continue
				
				params = parser["Desktop Entry"]
				
				if (
					params.get("Type") != "Application" or
					params.get("NoDisplay") == "true" or
					params.get("Hidden") == "true"
				):
					continue
				
				option = ethmenu.Option(
					caption=params.get("Name"),
					value=percent_sign_normalizer.sub("%",
						field_code_stripper.sub("",
							params.get("Exec")
						)
					),
					icon=params.get("Icon")
				)
				
				option.caption += f"<span font=\"0\">{option.value}</span>"
				
				options.append(option)
		
		options.sort(key=lambda option: option.caption)
		
		with open(cache_path, "w") as cache:
			json.dump([vars(option) for option in options], cache)
		
		for option in options:
			yield option

if __name__ == "__main__":
	main()
