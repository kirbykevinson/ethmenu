import os

def check(cache_path, cached_dirs):
	"""
	Checks if the cache is valid.
	
	# Arguments
	
	* `cache_path` - the path to the cache file
	* `cached_dirs` - the cached directories
	
	# Returns
	
	Whether or not the cache file last modification time is greater than
	the last modification times of the cached directories.
	"""
	
	try:
		cache_stat = os.stat(cache_path)
	except:
		return False
	
	for cached_dir in cached_dirs:
		try:
			cached_dir_stat = os.stat(cached_dir)
			
			if cache_stat.st_mtime <= cached_dir_stat.st_mtime:
				return False
		except:
			pass
	
	return True

def files(cached_dirs):
	"""
	The generator that lists the file contents of the directories.
	
	# Arguments
	
	* `cached_dirs` - the directories to to look into
	
	# Yields
	
	The filepaths of the files in the directories.
	"""
	
	for cached_dir in cached_dirs:
		try:
			cached_dir_contents = os.listdir(cached_dir)
			
			for entry in cached_dir_contents:
				entrypath = os.path.join(cached_dir, entry)
				
				if os.path.isfile(entrypath):
					yield entrypath
		except:
			pass
